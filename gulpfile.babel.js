import gulp from 'gulp';

// plugins
import autoprefixer from 'gulp-autoprefixer';
import plumber from 'gulp-plumber';
import minifyCSS from 'gulp-minify-css';
import notify from 'gulp-notify';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
import sass from 'gulp-sass';

const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

import babel from 'gulp-babel';
import gutil from 'gulp-util';
import webpack from 'webpack';
import webpackConfig from './webpack.config.babel';
import WebpackDevServer from 'webpack-dev-server';

import imagemin from 'gulp-imagemin';

import iconfont from 'gulp-iconfont';



// Paths config
const PATH = {
    src: './src/',
    dest: './dist/',

    srcJS: './src/js/**/*.js',
    srcSASS: './src/sass/**/*.scss',
    srcIMG: './src/img/**/*',
    srcICON: './src/icon/*.svg',

    destJS: './dist/js/',
    destTEMP: './dist/temp/',
    destCSS: './dist/css/',
    destIMG: './dist/img/',
    destFONT: './dist/fonts/'
};



gulp.task('default', ['webpack', 'sass', 'serve']);



gulp.task('serve', ['minify-css']['webpack'], () => {
    browserSync.init({
        server: "./"
    });

    gulp.watch(PATH.srcSASS, ['minify-css']);
    gulp.watch(PATH.srcJS, ['webpack']);
    gulp.watch(PATH.destCSS + 'style.css').on('change', reload);
    gulp.watch(PATH.destJS + 'bundle.js').on('change', reload);
    gulp.watch('./*.html').on('change', reload);
});




//CSS -------------------------------------------------

// Compile SASS
gulp.task('sass', () => {
    return gulp.src(PATH.srcSASS)
        .pipe(plumber())
        .pipe(sourcemaps.init())

        .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
        .pipe(autoprefixer({cascade: false }))
        .pipe(sourcemaps.write('./'), {includeContent: true})
        .pipe(gulp.dest(PATH.destCSS))
        .pipe(notify({ message: 'Styles task complete', onLast: true }));
});


// Minify CSS
gulp.task('minify-css',['sass'], () => {
    return gulp.src(PATH.destCSS + 'style.css')
        .pipe(minifyCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(PATH.destCSS))
        .pipe(notify({ message: 'minification task complete', onLast: true }));
});





// JS ----------------------------------------------

gulp.task('babel', () => {
    return gulp.src(PATH.srcJS)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(PATH.destTEMP));
});



gulp.task('webpack', ['babel'], function(callback) {
    var myConfig = Object.create(webpackConfig);
    myConfig.plugins = [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin()
    ];

    // run webpack
    webpack(myConfig, function(err, stats) {
        if (err) throw new gutil.PluginError('webpack', err);
        gutil.log('[webpack]', stats.toString({
            colors: true,
            progress: true
        }));
        callback();
    });
});


// Webpack server ------------------------------------------------------

gulp.task('server', ['webpack'], function(callback) {
    // modify some webpack config options
    var myConfig = Object.create(webpackConfig);
    myConfig.devtool = 'eval';
    myConfig.debug = true;

    // Start a webpack-dev-server
    new WebpackDevServer(webpack(myConfig), {
        publicPath: '/' + myConfig.output.publicPath,
        stats: {
            colors: true
        },
        hot: true
    }).listen(8080, 'localhost', function(err) {
            if(err) throw new gutil.PluginError('webpack-dev-server', err);
            gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
        });
});



// IMAGE Compression -------------------------------------------

gulp.task('compress-images', () => {
    return gulp.src(PATH.srcIMG)
        .pipe(imagemin({progressive: true, optimizationLevel: 7}))
        .pipe(gulp.dest(PATH.destIMG))
});



// Generate custom icon fonts -----------------------------------

gulp.task('iconfont', () => {

    return gulp.src([PATH.srcICONS])
        .pipe(iconfont({
            fontName: 'icon_font',
            appendCodepoints: true
        }))
        .on('codepoints', function () {
            console.log(codepoints, options);
        })
        .pipe(gulp.dest(PATH.destFONT));
});