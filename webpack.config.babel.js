import path from 'path';

module.exports = {
    entry: {
        preload: './dist/temp/main.js'
    },
    output: {
        path: path.join(__dirname, 'dist/js/'),
        publicPath: '../dist/js/',
        filename: 'bundle.js',
        chunkFilename: '[id].bundle.js'
    }
};

